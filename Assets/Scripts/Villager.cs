﻿using UnityEngine;
using System.Collections;

public class Villager : MonoBehaviour
{
	public float timeTillHouse = 5;
	public float buildingTime = 2;
	public float gatheringWait = 2;
	public float speed = 10;
	public float offset = 3.0f;
	public float afterFailure = 0.1f;
	public float gettingSmallerBy = 0.1f;
	public GameObject stoneInHand;
	public GameObject woodInHand;
	public GameObject villagerPrefab;
	public GameObject constructionSitePrefab;
	[HideInInspector]
	public Construction
		constructionSite = null;
	public AudioClip rockCollect;
	public AudioClip woodCut;
	GameObject closest;
	float timer = 0;
	float waitTimer = 0;
	Vector3 lastBuilt;
	Vector3 toGo;
	bool onceBuilt = false;
	bool notSet = true;
	bool canBuildHere = true;
	bool building = false;
	bool closestStoneUnknown = true;
	bool closestTreeUnknown = false;
	bool gathering = false;
	bool hasStone = false;
	bool wasInSite = false;
	bool noneExist = false;
	bool gettingSmaller = false;
	bool hasWood = false;

	void Start ()
	{
		transform.rotation = Quaternion.Euler (0, Random.Range (0, 360), 0);
		Registry.currentVillagers += 1;
		if (Registry.currentVillagers > Registry.maxVillagers) {
			Registry.currentVillagers -= 1;
			if (constructionSite != null) {
				constructionSite.DestroySelf ();
			}
			Destroy (gameObject);
			return;
		}
	}
	
	void Update ()
	{
		timer += Time.deltaTime;
		//--LOOKING FOR A PLACE TO BUILD
		if (timer >= timeTillHouse) {
			building = true;
			//--BUILDING
			if (constructionSite == null && !onceBuilt) {
				canBuildHere = true;
				foreach (Collider go in Physics.OverlapSphere (transform.position, 8)) {
					if (go.transform.root.tag == "Building" || go.gameObject.transform.root.tag == "Resource" || go.transform.root.tag == "Construction") {
						canBuildHere = false;
						transform.rotation = Quaternion.Euler (0, Random.Range (0, 360), 0);
						building = false;
						gathering = false;
						timer = 0;
						timeTillHouse += afterFailure;
						break;
					}
				}
				if (canBuildHere) {
					Registry.energyPoints += 100;
					constructionSite = ((GameObject)Instantiate (constructionSitePrefab, new Vector3 (transform.position.x + offset, transform.position.y, transform.position.z), Quaternion.identity)).GetComponent<Construction> ();
					if (notSet) {
						lastBuilt = new Vector3 (transform.position.x + offset, transform.position.y, transform.position.z);
						notSet = false;
					}
					onceBuilt = true;
				}
			}
			if (constructionSite != null)
				gathering = true;
		}

		if (gettingSmaller) {
			closest.transform.localScale -= new Vector3 (gettingSmallerBy, gettingSmallerBy, gettingSmallerBy);
			if (closest.transform.localScale.x <= 0.1f) {
				Destroy (closest);
				gettingSmaller = false;
			}
			gettingSmaller = false;
		}

	}

	void FixedUpdate ()
	{
		if (!building && !gathering) {
			//--LOOKING FOR A PLACE TO BUILD
			transform.position += transform.forward * Time.deltaTime * speed;
		} else if (gathering && building) {
			//--GATHERING RESOURCES
			if (closestStoneUnknown) {
				if (Registry.maxVillagers <= 15) {
					FindClosest ("Stone");
				} else {
					FindFirst ("Stone");
				}
			}
			if (closestTreeUnknown && Vector3.SqrMagnitude (transform.position - lastBuilt) <= 0.01f) {
				if (Registry.maxVillagers <= 15) {
					FindClosest ("Wood");
				} else {
					FindFirst ("Wood");
				}
			}
			if (Vector3.SqrMagnitude (transform.position - toGo) <= 0.01f) {
				if (!hasStone && !wasInSite && !hasWood && closest != null && constructionSite != null) {
					//--IN STONES IF OK
					transform.LookAt (toGo);
					gettingSmaller = true;
					hasStone = true;
					toGo = lastBuilt;
					PlaySound (rockCollect);
					stoneInHand.SetActive (true);
					waitTimer = 0;
				} else if (!hasStone && !wasInSite && !hasWood && closest == null) {
					//--IN STONES IF NOT OK
					hasStone = false;
					if (Registry.maxVillagers <= 15) {
						FindClosest ("Stone");
					} else {
						FindFirst ("Stone");
					}
				} else if (!wasInSite && !hasWood && hasStone && constructionSite != null) {
					stoneInHand.SetActive (false);
					closestTreeUnknown = true;
					if (!noneExist) {
						wasInSite = true;
					}
				} else if (wasInSite && hasStone && !hasWood && closest != null && constructionSite != null) {
					//--IN WOODS WHEN OK
					transform.LookAt (toGo);
					gettingSmaller = true;
					toGo = lastBuilt;
					PlaySound (woodCut);
					woodInHand.SetActive (true);
					hasWood = true;
					waitTimer = 0;
				} else if (wasInSite && hasStone && !hasWood && closest == null) {
					//--IN WOODS WHEN NOT OK
					if (Registry.maxVillagers <= 15) {
						FindClosest ("Wood");
					} else {
						FindFirst ("Wood");
					}
					hasWood = false;

				} else if (hasWood && hasStone && wasInSite && constructionSite != null) {
					//--FINISHED
					constructionSite.Build ();
					Registry.energyPoints += 100;
					Registry.currentVillagers -= 1;
					Destroy (gameObject);
				}
				if (constructionSite == null) {
					stoneInHand.SetActive (false);
					woodInHand.SetActive (false);
					if (Vector3.SqrMagnitude (transform.position - lastBuilt) >= 0.2f) {
						toGo = lastBuilt;
					} else {
						// cry about your house
						Instantiate (villagerPrefab, transform.position, transform.rotation);
						Registry.currentVillagers -= 1;
						Destroy (gameObject);
					}
				}
			} else {
				waitTimer += Time.deltaTime;
				if (waitTimer >= gatheringWait && !noneExist) {
					transform.LookAt (toGo);
					transform.position = Vector3.MoveTowards (transform.position, toGo, speed * Time.deltaTime);
				}
			}
		} else if (!gathering && building) {
			transform.LookAt (new Vector3 (lastBuilt.x, lastBuilt.y - offset, lastBuilt.z));
			transform.position = Vector3.MoveTowards (transform.position, new Vector3 (lastBuilt.x, lastBuilt.y - offset, lastBuilt.z), speed * Time.deltaTime);
		}
	}

	void FindClosest (string resourceTag)
	{
		closest = null;
		foreach (GameObject go in GameObject.FindGameObjectsWithTag(resourceTag)) {
			if (closest == null) {
				closest = go;
			} else {
				if (Vector3.SqrMagnitude (transform.position - go.transform.position) < Vector3.SqrMagnitude (transform.position - closest.transform.position)) {
					closest = go;
				}
			}
		}
		if (closest == null) {
			noneExist = true;
		} else {
			closestStoneUnknown = false;
			closestTreeUnknown = false;
			noneExist = false;
			toGo = new Vector3 (closest.transform.position.x + offset, closest.transform.position.y, closest.transform.position.z);
		}
	}

	void FindFirst (string resourceTag)
	{
		closest = GameObject.FindGameObjectWithTag (resourceTag);
		if (closest == null) {
			noneExist = true;
		} else {
			closestTreeUnknown = false;
			closestStoneUnknown = false;
			noneExist = false;
			toGo = new Vector3 (closest.transform.position.x + offset, closest.transform.position.y, closest.transform.position.z);
		}

	}

	void PlaySound (AudioClip clip)
	{
		AudioSource s = gameObject.GetComponent<AudioSource> ();
		s.PlayOneShot (clip);
	}
}


