﻿using UnityEngine;
using System.Collections;

public class LightningBolt : MonoBehaviour
{

	public float lifeTime = 1.0f;
	public AudioClip explosionSound;
	public int housePoints = 200;
	public int villagerPoints = 100;
	public int resourcesPoints = 30;
	public AudioClip golemDeath;
	float timer = 0;

	void Start ()
	{
		foreach (Collider go in Physics.OverlapSphere (transform.position, 2)) {
			if (go.gameObject.transform.root.tag == "Building") {
				Registry.PlaySound(explosionSound);
				Registry.currentHouses -= 1;
				Registry.destroyedOwnStructure = true;
				Registry.energyPoints -= housePoints;
				Destroy (go.gameObject.transform.root.gameObject);
			} else if (go.gameObject.transform.root.tag == "Villager") {
				Registry.PlaySound(explosionSound);
				Registry.energyPoints -= villagerPoints;
				Destroy (go.gameObject.transform.root.gameObject);
				Registry.currentVillagers -= 1;
			} else if (go.tag != "Ground" && go.tag != "Player" && go.tag != "Golem") {
				Registry.PlaySound(explosionSound);
				Registry.energyPoints -= resourcesPoints;
				Registry.destroyedOwnStructure = true;
				Destroy (go.gameObject.transform.parent.gameObject);
			} else if (go.tag == "Golem") {
				Registry.PlaySound(explosionSound);
				Registry.energyPoints += Registry.golemReward;
				Registry.PlaySound (golemDeath);
				Destroy (go.gameObject.transform.root.gameObject, golemDeath.length);
			}
		}
			
	}
	
	void Update ()
	{
		timer += Time.deltaTime;
		if (timer >= lifeTime) {
			Destroy (gameObject);
		}
	}

}
