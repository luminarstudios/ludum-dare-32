﻿using UnityEngine;
using System.Collections;

public class Registry
{
	public static int maxHouses = 1003;
	public static int currentHouses = 0;
	public static int housesLeftWave = 0;
	public static int housesLeftWin = 0;
	public static int waveHouses = 10;

	public static int currentWave = 1;

	public static int maxVillagers = 15;
	public static int currentVillagers = 0;

	public static int energyPoints = 0;
	public static int mode = 1;

	public static int flashCost = 500;
	public static int resourceCost = 300;
	public static int houseCost = 800;
	public static int golemReward = 100;
	
	public static bool destroyedOwnStructure = false;
	public static bool instructionsNeeded = true;

	public static int instructionsInt = 0;

	public static string [] hint = {"MOVE CAMERA WITH WASD","GREAT\nNOW TRY HOLDING THE RIGHT MOUSE BUTTON (RMB) AND MOVING THE MOUSE","NICE!\nYOU CAN SWITCH BETWEEN DIFFERENT MODES BY USING NUMBERS 1,2,3,4\n\nTRY THAT NOW!", "NOW WHEN YOU KNOW THE BASICS\n\nGOOD LUCK!", "USE LIGHTNING BOLTS TO KILL GOLEMS", "DO NOT DESTROY YOUR OWN VILLAGE"};

	public static void PlaySound (AudioClip clip) {
		AudioSource s = Camera.main.GetComponent<AudioSource> ();
		s.PlayOneShot (clip);
	}
	public static void Save ()
	{
		PlayerPrefs.SetInt ("InstructionsInt", instructionsInt);
	}

	public static void Load ()
	{
		instructionsInt = PlayerPrefs.GetInt("InstructionsInt");
	}
}
