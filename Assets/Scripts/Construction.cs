﻿using UnityEngine;
using System.Collections;

public class Construction : MonoBehaviour {

	public bool hasStone = false;
	public bool hasWood = false;
	public AudioClip buildSound;
	public GameObject buildingPrefab;

	void Start () {
	}
	
	void Update () {
	}

	public void Build ()
	{
		PlaySound (buildSound);

		Instantiate (buildingPrefab, transform.position, Quaternion.Euler(0,Random.Range (0,360),0));
		Destroy (gameObject);
	}

	public void DestroySelf ()
	{
		Destroy (gameObject);
	}

	void PlaySound (AudioClip clip) {
		AudioSource.PlayClipAtPoint (clip,transform.position, 0.25f);
	}
}
