﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Menu : MonoBehaviour {

	public Slider slider;
	public Text text;

	void Start () {
		Time.timeScale = 0;
	}
	
	void Update () {
		if (slider.value == 0) {
			text.text = "EASY: 400 houses";
			Registry.maxHouses = 403;
		} else if (slider.value == 1) {
			text.text = "MEDIUM: 600 houses";
			Registry.maxHouses = 603;
		} else if (slider.value == 2) {
			text.text = "HARD: 1000 houses";
			Registry.maxHouses = 1003;
		}
	}

	public void StartGame () {
		Application.LoadLevel ("scene01");
	}
}
