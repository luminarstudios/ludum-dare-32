﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainGame : MonoBehaviour
{
	public GameObject golem;
	public GameObject winText;
	public GameObject hintPanel;
	public Text hint;
	public AudioClip hintAudio;
	float distance = 100;
	int housesIncrease = 10;
	bool onceMovedCamera = false;
	bool onceTiltedCamera = false;
	bool destroyedOwnStructureOnce = false;

	void Start ()
	{
		Time.timeScale = 1;
		Registry.Load ();
		if (Registry.instructionsInt == 1) {
			Registry.instructionsNeeded = false;
		}
		if (Registry.instructionsNeeded) {
			ShowHint (0);
		}
	}

	void Update ()
	{
		Registry.housesLeftWave = Registry.waveHouses - Registry.currentHouses;
		Registry.housesLeftWin = Registry.maxHouses - Registry.currentHouses;

		if (Registry.housesLeftWin <= 0) {
			Registry.housesLeftWin = 0;
			winText.SetActive (true);
		}

		if (Registry.currentHouses >= Registry.waveHouses) {
			SpawnWave (Registry.currentWave, distance);
			if(Registry.currentWave == 1 && Registry.instructionsNeeded){
				ShowHint(4);
			}
			if (Registry.currentWave == 24){
				Registry.housesLeftWave = 1;
				Registry.currentWave = 24;
			}
			Registry.currentWave += 1;
			distance += 10;
			Registry.waveHouses += housesIncrease;
			housesIncrease += 3;
		}

		if (Registry.destroyedOwnStructure && !destroyedOwnStructureOnce) {
			ShowHint (5);
			destroyedOwnStructureOnce = true;
		}

		if (Registry.energyPoints < 0) {
			Registry.energyPoints = 0;
		}

		if (Input.GetKeyDown (KeyCode.Alpha1)) {
			Registry.mode = 1;
		}

		if (Input.GetKeyDown (KeyCode.Alpha2)) {
			Registry.mode = 2;
		}

		if (Input.GetKeyDown (KeyCode.Alpha3)) {
			Registry.mode = 3;
		}

		if (Input.GetKeyDown (KeyCode.Alpha4)) {
			Registry.mode = 4;
		}
		//-------------------
		if (Input.GetKey(KeyCode.F12))
			Registry.energyPoints += 100;
		//-------------------

		if (Input.GetKeyDown (KeyCode.H)) {
			hintPanel.SetActive (false);
		}
		if (Registry.instructionsNeeded) {
			if (Input.GetKeyDown (KeyCode.Alpha2) || Input.GetKeyDown (KeyCode.Alpha3) || Input.GetKeyDown (KeyCode.Alpha4)){
				ShowHint(3);
				Registry.instructionsNeeded = false;
				Registry.instructionsInt = 1;
				Registry.Save ();
			}

			if (Input.GetKeyDown (KeyCode.W) && !onceMovedCamera || Input.GetKeyDown (KeyCode.A) && !onceMovedCamera || Input.GetKeyDown (KeyCode.S) && !onceMovedCamera || Input.GetKeyDown (KeyCode.D) && !onceMovedCamera) {
				onceMovedCamera = true;
				ShowHint (1);
			}

			if (Input.GetButton ("Fire2") && !onceTiltedCamera) {
				onceTiltedCamera = true;
				ShowHint (2);
			}
		}
	}

	void SpawnWave (float wave, float position)
	{
		for (int i = 0; i < wave; i++) {
			Instantiate (golem, new Vector3 (position + Random.Range (-100,100), 0, position), Quaternion.identity);
			position += 30;
		}
	}

	void ShowHint (int arrayPos)
	{
		Registry.PlaySound (hintAudio);
		hint.text = Registry.hint [arrayPos];
		hintPanel.SetActive (true);
	}
}
