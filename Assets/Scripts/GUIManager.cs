﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GUIManager : MonoBehaviour
{

	public Text pointText;
	public GameObject mode1Panel;
	public GameObject mode2Panel;
	public GameObject mode3Panel;
	public GameObject mode4Panel;
	public Text boltText;
	public Text stoneText;
	public Text treeText;
	public Text houseText;
	public Text waveHouses;
	public Text housesToWin;
	public Image waveIndicator;

	void Start ()
	{
		boltText.text = "" + Registry.flashCost;
		stoneText.text = "" + Registry.resourceCost;
		treeText.text = "" + Registry.resourceCost;
		houseText.text = "" + Registry.houseCost;
	}
	
	void Update ()
	{
		waveHouses.text =  "BUILD " + Registry.housesLeftWave + " HOUSES FOR WAVE " + Registry.currentWave;
		housesToWin.text = "BUILD " + Registry.housesLeftWin + " HOUSES TO WIN";
		pointText.text = "" + Registry.energyPoints;
		//waveIndicator.transform.localScale = new Vector3 ( 1/(Registry.offset / (Registry.waveHouses - Registry.currentHouses)), 1, 0);

		if (Registry.mode == 1) {
			mode1Panel.SetActive (true);
			mode2Panel.SetActive (false);
			mode3Panel.SetActive (false);
			mode4Panel.SetActive (false);
		} else if (Registry.mode == 2) {
			mode1Panel.SetActive (false);
			mode2Panel.SetActive (true);
			mode3Panel.SetActive (false);
			mode4Panel.SetActive (false);
		} else if (Registry.mode == 3) {
			mode1Panel.SetActive (false);
			mode2Panel.SetActive (false);
			mode3Panel.SetActive (true);
			mode4Panel.SetActive (false);
		} else if (Registry.mode == 4) {
			mode1Panel.SetActive (false);
			mode2Panel.SetActive (false);
			mode3Panel.SetActive (false);
			mode4Panel.SetActive (true);
		}
	}
}
