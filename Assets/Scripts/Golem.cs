﻿using UnityEngine;
using System.Collections;

public class Golem : MonoBehaviour
{

	public float speed = 10;
	public float offset = 3;
	public float gettingSmallerBy = 0.01f;
	public AudioClip golemEating;
	public AudioClip golemDeath;
	float radius = 20;
	GameObject food;
	bool gettingSmaller = false;
	bool foundFood = false;
	bool oncePlayed = false;
	bool eating = false;
	Vector3 foodPosition;

	void Start ()
	{
		FindFood ();
	}
	
	void Update ()
	{
		if (gettingSmaller && food != null) {
			if (food.transform.localScale.x > 0.0f) {
				food.transform.localScale -= new Vector3 (gettingSmallerBy, gettingSmallerBy, gettingSmallerBy);
			} else {
				gettingSmaller = false;
			}
		}
	}

	void FixedUpdate ()
	{
		if (foundFood) {
			if (food != null) {
				transform.LookAt (food.transform.position);
				if (Vector3.SqrMagnitude (transform.position - foodPosition) >= 0.01f) {
					if (!eating) {
						transform.position = Vector3.MoveTowards (transform.position, foodPosition, speed * Time.deltaTime);
					}
				} else {
					eating = true;
					if(!oncePlayed){
						oncePlayed = true;
						Registry.PlaySound(golemEating);
					}
					gettingSmaller = true;
					if (food.transform.localScale.x <= 0.0f) {
						Registry.energyPoints -= 50;
						foundFood = false;
						eating = false;
						Destroy (food);
						oncePlayed = false;
						gettingSmaller = false;
						FindFood ();
					} 
				}
			} else {
				foundFood = false;
				radius += 10;
				FindFood ();
			}
		} 

	}
	
	void FindFood ()
	{
		GameObject closest = null;
		foreach (Collider col in Physics.OverlapSphere(transform.position, radius)) {
			if (col.gameObject.transform.root.tag == "Player" || col.gameObject.transform.root.tag == "Ground" || col.gameObject.transform.root.tag == "Villager" || col.gameObject.transform.root.tag == "Golem" || col.gameObject.transform.root.tag == "Resource") {
			} else {
				if (closest == null || Vector3.SqrMagnitude (transform.position - col.gameObject.transform.parent.position) < Vector3.SqrMagnitude (transform.position - closest.transform.position)) {
					closest = col.gameObject;
					foodPosition = new Vector3 (col.gameObject.transform.position.x + offset, 0, col.gameObject.transform.position.z + offset);
					food = col.gameObject.transform.parent.gameObject;
				}
			}
		}

		foundFood = true;
	}

	void DestroySelf ()
	{
		Registry.PlaySound (golemDeath);
		Destroy (gameObject, golemDeath.length);
	}
}
