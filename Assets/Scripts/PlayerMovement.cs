﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

	public float mouseSensitivity = 3f;
	public float rotUp = 0;
	public float rotDown = 90;
	public float scrollSpeed = 10f;
	public float top = 100f;
	public float bottom = 20f;

	float verticalRotation = 70;

	void Start () {
	}
	
	void Update () {
		if (Input.GetButton ("Fire2")) {
			// Rotate camera
			verticalRotation += Input.GetAxis("Mouse Y") * mouseSensitivity;
			verticalRotation = Mathf.Clamp(verticalRotation, rotUp, rotDown);
			Camera.main.transform.localRotation = Quaternion.Euler(verticalRotation, 0, 0);

			//transform.Rotate(0, -Input.GetAxis("Mouse X") * mouseSensitivity, 0);
		}

		if (Input.GetButton ("Fire3")) {
			// Move player
			transform.Translate(-Input.GetAxis("Mouse X") * transform.position.y / 20, 0, -Input.GetAxis("Mouse Y") * (transform.position.y / 20));
		}

		Vector3 newPos = transform.position + Camera.main.transform.forward * Input.GetAxis("Mouse ScrollWheel") * scrollSpeed * (transform.position.y / 30);
		newPos.y = Mathf.Clamp (newPos.y, bottom, top);

		transform.position = newPos;

		transform.Translate(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

	}
}
