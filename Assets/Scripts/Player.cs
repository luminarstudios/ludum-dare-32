﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
	public GameObject boltPrefab;
	public GameObject rockPrefab;
	public GameObject treePrefab;
	public GameObject housePrefab;
	public AudioClip boltSound;

	void Start ()
	{
	
	}
	
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Escape)) {
			Application.Quit ();
		}
		if (Input.GetButtonDown ("Fire1")) {
			if (Registry.mode == 1) {
				if (Registry.energyPoints >= Registry.flashCost) {
					Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
					RaycastHit hit;
					if (Physics.Raycast (ray, out hit)) {
						Registry.energyPoints -= Registry.flashCost;
						Instantiate (boltPrefab, hit.point, Quaternion.identity);
						Registry.PlaySound (boltSound);
					}
				}
			} else if (Registry.mode == 2){
				if (Registry.energyPoints >= Registry.resourceCost) {
					Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
					RaycastHit hit;
					if (Physics.Raycast (ray, out hit)) {
						Registry.energyPoints -= Registry.resourceCost;
						Instantiate (rockPrefab, new Vector3(hit.point.x, 0, hit.point.z), Quaternion.identity);
						//Registry.PlaySound (boltSound);
					}
				}
			} else if (Registry.mode == 3){
				if (Registry.energyPoints >= Registry.resourceCost) {
					Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
					RaycastHit hit;
					if (Physics.Raycast (ray, out hit)) {
						Registry.energyPoints -= Registry.resourceCost;
						Instantiate (treePrefab, new Vector3(hit.point.x, 0, hit.point.z), Quaternion.identity);
						//Registry.PlaySound (boltSound);
					}
				}
			} else if (Registry.mode == 4){
				if (Registry.energyPoints >= Registry.houseCost) {
					Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
					RaycastHit hit;
					if (Physics.Raycast (ray, out hit)) {
						Registry.energyPoints -= Registry.houseCost;
						Instantiate (housePrefab, new Vector3(hit.point.x, 0, hit.point.z), Quaternion.identity);
						//Registry.PlaySound (boltSound);
					}
				}
			}
		}
	}
}
