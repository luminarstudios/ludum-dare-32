﻿using UnityEngine;
using System.Collections;

public class Building : MonoBehaviour
{
	public GameObject villagerPrefab;
	public GameObject upgradedHousePrefab;
	public float timeTillUpgrade = 20;
	public AudioClip upgradeSound;
	float upgradeTimer = 0;
	float timer = 0;
	float maxTime = 5.0f;
	bool upgraded = false;

	void Start ()
	{
		Registry.currentHouses += 1;
		maxTime = maxTime + Random.Range (-2, 2);
		timeTillUpgrade = timeTillUpgrade + Random.Range (-10, 10);
		if(Random.Range(1,3) == 2 && (float) Registry.currentHouses / (float) Registry.maxHouses >= 0.20f){
			upgraded = true;
		}
	}
	
	void Update ()
	{
		timer += Time.deltaTime;
		if (!upgraded) {
			upgradeTimer += Time.deltaTime;
		}

		//Debug.Log ( Registry.currentHouses + " / " + Registry.maxHouses);

		if (timer >= maxTime && Registry.currentHouses < Registry.maxHouses) {
			maxTime += Random.Range (-2.6f, 5.4f);
			maxTime += Random.Range (-1.1111f, 1.2222f);
			maxTime += Random.Range (-0.11154f, 0.15464f);
			timer = 0;
			if (Registry.currentVillagers < Registry.maxVillagers) {
				InstantiateVillager ();
			}
		} else if (timer >= maxTime && Registry.currentHouses >= Registry.maxHouses) {
			timer = 0;
		}
		if (upgradeTimer >= timeTillUpgrade && (float) Registry.currentHouses / (float) Registry.maxHouses >= 0.3f && !upgraded) {
			Destroy(gameObject.transform.GetChild(0).gameObject);
			GameObject go = (GameObject) Instantiate(upgradedHousePrefab, transform.position, transform.rotation);
			go.transform.SetParent(gameObject.transform.root);
			Registry.energyPoints += 50;
			PlaySound (upgradeSound);
			upgraded = true;
		}
		if (upgradeTimer >= timeTillUpgrade && !upgraded) {
			upgradeTimer = 0;
		}
	}

	void InstantiateVillager ()
	{
		Instantiate (villagerPrefab, new Vector3 (transform.position.x + 2.0f, transform.position.y, transform.position.z), Quaternion.identity);
		maxTime += 1;
		timer = 0;
	}

	void PlaySound (AudioClip clip) {
		AudioSource s = gameObject.GetComponent<AudioSource> ();
		s.PlayOneShot (clip);
	}

}
